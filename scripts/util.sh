#!/usr/bin/env bash

# 0. Start out with scripts/ as our working directory
cd "$(dirname "$0")" || exit 1

# 0a Read project's environment variables
set -o allexport; source ../.env; set +o allexport

LAMBDA_FUNCTION_DEPLOY=$1
DIR_NAME=$2

# 1. Make all of our source code available to Lambda
cp -r ../cli/src/* ../"$DIR_NAME"
cp ../cli/package.json ../"$DIR_NAME"/package.json
cp ../cli/package-lock.json ../"$DIR_NAME"/package-lock.json
cp ../.env ../"$DIR_NAME"/.env
cp ../"$DIR_NAME"/index.js ../"$DIR_NAME"/index2.js
cp ../"$DIR_NAME"/lambda-entrypoint.js ../"$DIR_NAME"/index.js

# 2. Bundle
BUNDLE_NAME="bundle"
cd ../"$DIR_NAME" || exit 1
npm install
zip -r "$BUNDLE_NAME.zip" .

# Deploy
aws lambda update-function-code --function-name "$LAMBDA_FUNCTION_DEPLOY" \
--zip-file "fileb://$(pwd)/$BUNDLE_NAME.zip" --region us-east-1

# Cleanup
rm "$BUNDLE_NAME.zip"
