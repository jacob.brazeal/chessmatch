# Bring Your Own Chess Engine CLI

(Pre-alpha) An educational tool that allows you to try different sets of parameters on a pre-build chess engine and run
tournaments between the resulting bots.

## API

### Run a match between two engines

`chessmatch match -w whitePlayer -b blackPlayer -n <rounds> [-c config-file.yaml]`

### Run a Swiss tournament between multiple engines

`chessmatch swiss -e comma,separated,engine,list -n <rounds> [-c config-file.yaml]`

## Config format

You can provide the name of a YAML engine config file to the commands via the `-c` option if you want to customize the
behavior of the engines. By default, the file in `configs/default.yaml` is used.

A config file is a list of named engines. See `cli/configs` for some example configs. By default, you're using the
engines defined in `configs/default.yaml`.

Each engine config has options including:

* `evalFn` (required) as one of the following options: `random`, `e1`, or `e2`.
* `params` (optional) customize the behavior of a given evaluation function
    * e1 params:
        * `checkWeight`: An integer that encourages aggressive checks (higher = more aggressive, 0 = not considered).
        * `checkmateWeight`: An integer that encourages checkmates (higher = checkmate weighted more aggressively.)
