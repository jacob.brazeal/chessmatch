const {engineFactory} = require("./util");
const {gameTree} = require("./util");
const {getDescendants} = require("./util");

function positionScore(chessObject, options) {
    options = Object.assign({
        pieceEvaluation: {
            'q': 9,
            'b': 3,
            'n': 3,
            'r': 5,
            'p': 1,
            'k': 0
        },
        checkWeight: 3,
        checkmateWeight: 100


    }, options);

    const turnMultiplier = chessObject.turn() === 'w' ? 1 : -1;

    if (chessObject.inCheckmate() && options.checkmateWeight) {
        return -1 * turnMultiplier * options.checkmateWeight;
    }

    if (chessObject.inDraw()) {
        return 0;
    }

    let eval = 0;
    for (const row of chessObject.board()) {
        for (const piece of row) {
            if (!piece) continue;
            const sign = piece.color === 'w' ? 1 : -1;
            eval += sign * options.pieceEvaluation[piece.type];
        }
    }

    if (chessObject.inCheck()) {
        eval += -1 * turnMultiplier * options.checkWeight;
    }


    return eval;
}

exports.e1 = engineFactory(positionScore);
