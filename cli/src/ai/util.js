const Chess = require('@ninjapixel/chess').Chess;

const getDescendants = exports.getDescendants = function getDescendants(chessObject) {
    const moves = chessObject.moves();
    return moves.map(m => {
        const child = new Chess(chessObject.fen())
        child.move(m);
        return {
            move: m,
            child: child
        };
    })
}

const gameTree = exports.gameTree = function gameTree(chessObject, options) {
    options = Object.assign({
        depth: 1,
        branchFactor: 3,
        evalFn: () => 0,
        evalOptions: {}
    }, options);

    const isWhite = chessObject.turn() === 'w';
    // 0). Candidate moves
    const descendants = getDescendants(chessObject).sort(() => Math.random() - 0.5);
    descendants.sort((a, b) => (
        options.evalFn(b.child, options.evalOptions) - options.evalFn(a.child, options.evalOptions))); // High evals first
    if (!isWhite) descendants.reverse(); // Favorable evals first
    const candidates = descendants.slice(0, options.branchFactor);

    if (!candidates.length) {
        // We can't go deeper in the tree
        return {
            move: null,
            eval: options.evalFn(chessObject, options.evalOptions)
        }
    }

    if (options.depth <= 1) {
        return {
            move: descendants[0].move,
            eval: options.evalFn(descendants[0].child, options.evalOptions)
        }
    }

    // 1) Game tree
    let bestMove = null, bestEval = null;
    for (const {move, child} of candidates) {
        let {eval} = gameTree(child, {...options, depth: options.depth - 1});
        if (bestMove === null || (isWhite && eval > bestEval) || (!isWhite && eval < bestEval)) {
            bestMove = move;
            bestEval = eval;
        }
    }
    return {
        move: bestMove,
        eval: bestEval
    }


}

exports.engineFactory = (evalFnFamily) =>
    (engineSetupOptions) => {
        engineSetupOptions = Object.assign({
            depth: 1,
            branchFactor: 10,
            evalOptions: {},
        }, engineSetupOptions)

        return function (chessObject, options) {
            options = Object.assign({
                depth: engineSetupOptions.depth,
                branchFactor: engineSetupOptions.branchFactor,
                evalFn: evalFnFamily,
                evalOptions: engineSetupOptions.evalOptions
            }, options);

            const {move, eval} = gameTree(chessObject, options);
            // console.log(chessObject.ascii());
            // console.log(eval)
            return move;

        }
    }

