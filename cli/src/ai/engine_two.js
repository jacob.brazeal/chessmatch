const {engineFactory} = require("./util");
const {gameTree} = require("./util");
const {getDescendants} = require("./util");

function positionScore(chessObject, options) {
    options = Object.assign({
        pieceEvaluation: {
            'q': 9,
            'b': 3,
            'n': 3,
            'r': 5,
            'p': 1,
            'k': 0
        },
        checkWeight: 3,
        kingSafety: 1.5,
        pawnAdvanceBonus: 0.4,
        centreBonus: 1 // Value pieces near the center more highly


    }, options);

    const turnMultiplier = chessObject.turn() === 'w' ? 1 : -1;
    const isWhite = chessObject.turn() === 'w';

    if (chessObject.inCheckmate()) {
        return -1 * turnMultiplier * 100;
    }

    if (chessObject.inDraw()) {
        return 0;
    }

    let eval = 0;
    let r = 0;
    for (const row of chessObject.board()) {
        let c = 0;
        for (const piece of row) {
            if (!piece) continue;
            const sign = piece.color === 'w' ? 1 : -1;

            const centerBonus = r >= 3 && r <= 4 && c >= 3 && c <= 4 ? options.centreBonus : 1;
            eval += sign * options.pieceEvaluation[piece.type] * centerBonus;

            if (piece.type === 'p') {
                const adv = isWhite ? (r - 2) * options.pawnAdvanceBonus : (7 - r) * options.pawnAdvanceBonus;
                eval += adv;
            }
            c += 1
        }
        r += 1;
    }

    if (chessObject.inCheck()) {
        eval += -1 * turnMultiplier * options.checkWeight;
    }


    return eval;
}

exports.e2 = engineFactory(positionScore);
