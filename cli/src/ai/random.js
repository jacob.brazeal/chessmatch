exports.randomAI = function(chessBoard) {
    const moves = chessBoard.moves();
    if ( !moves.length) throw "No moves";
    const move = moves[Math.floor(Math.random() * moves.length)];
    return move;

}
