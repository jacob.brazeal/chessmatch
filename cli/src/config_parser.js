const YAML = require('yaml')
const fs = require('fs');
const path = require("path");
const {e2} = require("./ai/engine_two");
const {e1} = require("./ai/engine_one");
const {randomAI} = require("./ai/random");

function readConfig(options = {}) {
    if (!options.filename && !options.config) {
        console.log("You must provide either a config filename or a config string");
        process.exit(1);
    }

    if (options.config) {
        return options.config;
    }

    // Read and parse config
    let configContents;
    try {
        configContents = fs.readFileSync(options.filename).toString();
    } catch (e) {
        console.log("Unable to read file with config at " + options.filename);
        process.exit(1);
    }

    const parsed = YAML.parse(configContents);

    return parsed;

}

const evalFnMap = {
    'random': () => randomAI,
    'e1': e1,
    'e2': e2
}

exports.configParser = function (options) {
    options = Object.assign({
        filename: null,
        config: null
    }, options);

    const config = readConfig(options);

    // Validate
    //  1) At least one participant
    //  2) Participants uniquely named
    //  3) Every participant specifies a valid evalFn
    if (!Object.keys(config).length) {
        console.log("You must provide at least one engine in the config file");
        process.exit(1);
    }
    for (const [name, engineConfig] of Object.entries(config)) {
        if (!evalFnMap[engineConfig.evalFn]) {
            console.log(`Engine ${name} has an evalFn of '${engineConfig.evalFn}', but expected one of ${Object.keys(evalFnMap)}`)
        }
        const options = {
            depth: engineConfig.depth,
            branchFactor: engineConfig.branchFactor,
            evalOptions: engineConfig.params
        }
        // Construct engines with the appropriate parameters
        engineConfig.engine = evalFnMap[engineConfig.evalFn](options);
    }

    // console.log("Config: ", config);
    return config;


}

exports.getConfig = function (filenameOrObject) {
    if (filenameOrObject && typeof filenameOrObject === 'object') {
        return exports.configParser({config: filenameOrObject});
    }
    let filename = filenameOrObject || path.resolve(__dirname, '../', 'configs/default.yaml');
    return exports.configParser({filename: filename})
}

