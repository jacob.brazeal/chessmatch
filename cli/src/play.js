const {engine1AI} = require("./ai/engine_one");
const Chess = require('@ninjapixel/chess').Chess;
const {randomAI} = require('./ai/random');

exports.play = function (options) {

    const chess = new Chess();

    options = Object.assign({
        whiteAI: engine1AI,
        blackAI: randomAI
    }, options);

    let curTurn = 0;
    let moveNum = 0;
    let ais = [options.whiteAI, options.blackAI];
    while (!chess.gameOver()) {

        const move = ais[curTurn](chess);

        chess.move(move);
        curTurn = 1 - curTurn;
        if (curTurn === 0) {
            ++moveNum;
        }
    }

    const result = chess.inDraw() ? '1/2-1/2' : curTurn === 1 ? '1-0' : '0-1';

    console.log("Result: ", result, "; moves: " + moveNum);
    console.log(chess.ascii());

    return {
        result: result,
        moves: moveNum,
        pgn: chess.pgn()
    };
}
