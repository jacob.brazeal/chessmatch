exports.FORMATS = {
    MATCH: 'match',
    SWISS: 'swiss'
}

exports.RESULTS = {
    WHITE_WIN: '1-0',
    BLACK_WIN: '0-1',
    DRAW: '1/2-1/2'
}
