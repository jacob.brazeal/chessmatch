const {getConfig} = require("../config_parser");
const {play} = require("../play");
const {RESULTS} = require("../constants");

exports.swiss = function swiss(options) {
    /* Match-specific options */
    options = Object.assign({
        rounds: 1,
        engines: 'random,simple',
        config: null
    }, options);

    const config = getConfig(options.config);


    // Stats
    const results = {[RESULTS.WHITE_WIN]: 0, [RESULTS.BLACK_WIN]: 0, [RESULTS.DRAW]: 0};
    const moveDistribution = [];

    // Input validation
    const engines = Array.from(new Set(options.engines.split(',')));
    if (engines.filter(x => !config[x]).length) {
        console.log(`Each engine given must be a member of the list: ${Object.keys(config)}`);
        process.exit(1);
    }
    if (engines.length % 2 !== 0) {
        console.log("Please provide an even number of engines for Swiss tournaments");
        process.exit(1);
    }

    const scores = Object.fromEntries(engines.map(x => [x, {
        score: 0,
        games: 0
    }]));
    const gameResults = [];

    // A fake-swiss tournament because we allow repeat matches
    for (let i = 0; i < options.rounds; ++i) {

        // Produce matchups
        const inOrderDescending = Object.entries(scores)
            .sort((a, b) => b[1].score - a[1].score);
        const matchups = [];
        console.log(inOrderDescending);
        for (let j = 0; j < inOrderDescending.length; j += 2) {
            matchups.push([inOrderDescending[j][0], inOrderDescending[j + 1][0]].sort((a, b) => Math.random() - 0.5)); // Randomly order b/w
        }
        console.log(matchups);

        for (const [black, white] of matchups) {
            console.log(`Round ${i + 1}/${options.rounds}: White (${white}) vs Black (${black})`);
            const {result, moves, pgn} = play({
                whiteAI: config[white].engine,
                blackAI: config[black].engine
            })

            // Let's record who won.
            gameResults.push({
                round: i,
                black,
                white,
                result,
                moves,
                pgn
            });
            scores[white].games++;
            scores[black].games++;

            // Represent 1/2 point as 1 and 1 point as 2 to avoid issues with rounding
            if (result === RESULTS.WHITE_WIN) {
                scores[white].score += 2;
            } else if (result === RESULTS.BLACK_WIN) {
                scores[black].score += 2;
            } else {
                scores[white].score += 1;
                scores[black].score += 1;
            }
            results[result] += 1;
            moveDistribution.push(moves);

            console.log(scores);

        }

    }

    console.log("Average moves: ", moveDistribution.reduce((a, b) => a + b) / moveDistribution.length);
    console.log("Results: ", results);
    console.log("Scores: ", scores);


}
