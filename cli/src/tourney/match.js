const {getConfig} = require("../config_parser");
const {play} = require("../play");
const {RESULTS} = require("../constants");

exports.match = function match(options) {
    /* Match-specific options */
    options = Object.assign({
        rounds: 1,
        white: 'random',
        black: 'random',
        config: null
    }, options);

    const config = getConfig(options.config);

    // Stats
    const results = {[RESULTS.WHITE_WIN]: 0, [RESULTS.BLACK_WIN]: 0, [RESULTS.DRAW]: 0};
    const moveDistribution = [];

    // Input validation
    if (!(config[options.white] && config[options.black])) {
        console.log(`The players specified as white and black must be named in the config file. Currently available: ${Object.keys(config)}`);
        process.exit(1);
    }

    // Run the match for specified number of rounds
    for (let i = 0; i < options.rounds; ++i) {
        console.log(`Round ${i + 1}/${options.rounds}: White (${options.white}) vs Black (${options.black})`)
        const {result, moves, pgn} = play({
            whiteAI: config[options.white].engine,
            blackAI: config[options.black].engine
        })

        if (options.pgn) {
            console.log(pgn);
        }

        results[result] += 1;
        moveDistribution.push(moves);
        console.log(results);
    }

    console.log("Average moves: ", moveDistribution.reduce((a, b) => a + b) / moveDistribution.length);
    console.log(moveDistribution.join(' '));
    console.log("Results: ", results);

    return results;

}
