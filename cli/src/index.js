require('dotenv').config();

const yargs = require('yargs');
const path = require("path");
const {swiss} = require("./tourney/swiss");
const {match} = require("./tourney/match");
const {FORMATS} = require('./constants');

const argv = yargs
    .command(['match'], 'Run two AIs against each other', {
        white: {
            description: 'White AI engine: random|simple',
            alias: 'w',
            required: false,
            type: 'string',
            default: 'random'
        },
        black: {
            description: 'Black AI engine: random|simple',
            alias: 'b',
            required: false,
            type: 'string',
            default: 'random'
        },
        rounds: {
            description: 'Number of rounds in match',
            alias: 'n',
            required: false,
            type: 'string',
            default: 5
        },
        pgn: {
            description: 'Output pgn',
            alias: 'p',
            required: false,
            type: 'boolean',
            default: false
        },
        config: {
            description: 'Engine config file',
            alias: 'c',
            required: false,
            type: 'string'
        }

    })
    .command(['swiss'], 'Run a Swiss tournament between AIs', {
        engines: {
            description: 'Comma-separated list of engines',
            alias: 'e',
            required: true,
            type: 'string',
        },
        rounds: {
            description: 'Number of rounds in tournament',
            alias: 'n',
            required: false,
            type: 'string',
            default: 5
        },
        config: {
            description: 'Engine config file',
            alias: 'c',
            required: false,
            type: 'string'
        },
        htmlWatch: {
            description: 'File to output HTML description of progress',
            alias: 'h',
            required: false,
            type: 'string'
        }

    })

    .help()
    .alias('help', 'h')
    .argv;



async function main() {

    if (!argv._.length) {
        console.log("Run with --help for usage options.")
        return;
    }

    if (argv._[0] === FORMATS.MATCH) {
        return match({...argv});
    } else if (argv._[0] === FORMATS.SWISS) {
        return swiss({...argv});
    } else {
        throw `Tourney format ${argv._[0]} currently not supported`
    }

}


main();
