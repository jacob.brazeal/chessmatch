# Bring Your Own Chess Engine

By Jacob Brazeal (jacob.brazeal@gmail.com)

This is an educational project that allows you to create your own chess engine by tuning a set of parameters for a
standard minimax search, and let groups of engines with different parameters compete against each other. This project
includes

- a command-line utility for tuning + tournaments

Planned:

- a server to expose the CLI
- a web interface to interact with the server
- pre-built AWS cloud architecture to deploy the tournament.




