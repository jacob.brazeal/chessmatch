const tableName = 'chessmatchui-executions'

exports.handler = async (event, context) => {
    // Currently only support matches
    let payload = Object.assign({
        config: null,
        white: 'random',
        black: 'random'
    }, JSON.parse(event.body));
    const batchId = event.batchId || '-';
    const batchSize = event.batchSize || 1;
    const jobId = event.jobId || '-';
    console.log("Loaded payload: ", payload);
    console.log('BatchID: ', batchId, 'BatchSize:', batchSize, 'jobId: ', jobId);

    // 1. Create temp entry in DDB
    const DynamoDB = require('aws-sdk/clients/dynamodb')
    const ddb = new DynamoDB({
        apiVersion: '2012-08-10',
    });

    let params = {
        TableName: tableName,
        Item: {
            'jobId': {S: jobId},
            'batchId': {S: batchId},
            'result': {S: ''},
            'status': {S: 'processing'},
            'date': {N: new Date().getTime().toString()},
            'white': {S: payload.white},
            'black': {S: payload.black},
            'batchSize': {N: batchSize.toString()}
        }
    };

    await ddb.putItem(params).promise();


    let output;
    try {
        const {match} = require('./tourney/match');
        output = await match(payload);

    } catch (e) {
        console.error(e);
        output = {
            error: true
        }
    }

    // Report result to DDB
    params = {
        TableName: tableName,
        Key: {
            "jobId": {S: jobId}
        },
        UpdateExpression: "set #status = :x, #result = :y",
        ExpressionAttributeNames: {
            '#status': 'status',
            '#result': 'result'
        },
        ExpressionAttributeValues: {
            ":x": {S: 'done'},
            ":y": {S: JSON.stringify(output)},
        }
    };

    await ddb.updateItem(params).promise();


}
